package com.citi.trading;

import org.springframework.stereotype.Component;

import java.util.function.Consumer;

public class MockMarket implements OrderPlacer {

    public MockMarket() {

    }

    public void placeOrder(Trade order, Consumer<Trade> callback) {
        int shares = order.getSize();
        order.setResult(Trade.Result.FILLED);
        order.setSize(shares);
        callback.accept(order);
    }

}
