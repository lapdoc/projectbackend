package com.citi.trading.strategy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.citi.trading.WebExceptions;

/**
 * REST web service allowing control of traders. The terminology is a bit
 * shifty here. Throughout the application, we refer to a {@link Strategy} as
 * the persistent state that defines parameters for a particular algorithm,
 * and to a {@link Trader} as the algorithm implementation, which refers to
 * the strategy for its operating parameters. This service works with both 
 * types. At its core it's a CRUD service over strategies; but it is proactive
 * and for example will create the new trader for a new strategy and will 
 * put that trader into action. For the caller, it's all just state 
 * representations, so the caller is really working with strategy objects.
 * 
 * @author Will Provost
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/traders")
public class TraderService {

	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private ActiveTraders activeTraders;
	
	/**
	 * Helper to return an HTTP 404 if the given strategy ID is not found
	 * in the database.
	 */
	private Strategy findOrFail(int ID) {
		Strategy strategy = strategyRepository.findStrategyAndPositions(ID);
		if (strategy == null) {
			throw new WebExceptions.NotFound("No strategy with ID=" + ID + ".");
		}
		
		return strategy;
	}
	
	/**
	 * Get all strategies -- active and inactive.
	 */
	@GetMapping
	public List<Strategy> getTraders() {
		ArrayList<Strategy> result = new ArrayList<>();
		for (Strategy strategy : strategyRepository.findAll()) {
			result.add(strategy);
		}
		return result;
	}
	
	/**
	 * Gets a strategy by ID. 
	 */
	@GetMapping("{ID}")
	public Strategy getTraderById(@PathVariable("ID") int ID) {
		return findOrFail(ID);
	}
	
	/**
	 * Update a strategy, setting its <strong>active</strong> property to the
	 * given true/false value. This triggers addition to or removal from the
	 * associated {@link ActiveTraders} component, so that an activated trader
	 * will immediately start trading, and a deactivated one will stop.
	 */
	@PutMapping("{ID}/active")
	@Transactional
	public void activateTrader(@PathVariable("ID") int ID, @RequestBody String start) {

		Strategy strategy = findOrFail(ID);
		strategy.setActive(start.equals("true"));
		strategy = strategyRepository.save(strategy);

		if (strategy.isActive()) {
			activeTraders.addTraderFor(strategy);
		} else {
			activeTraders.removeTraderFor(strategy);
		}
	}
	
	/**
	 * Creates a new strategy, and then creates the associated trader and adds it to
	 * the associated {@link ActiveTraders} component so that it will start trading.
	 */
	@PostMapping("{isReal}")
	@ResponseStatus(HttpStatus.CREATED)
	public Strategy saveStrategy(@PathVariable String isReal, @RequestBody Strategy newStrategy) {
		if (newStrategy.getId() != 0) {
			throw new WebExceptions.Conflict
				("Do not specify an ID for a new object; you may inadvertently overwrite an existing object.");
		}

		Strategy saved = newStrategy;
		if(isReal.equals("true")) {
		    saved.setReal(true);
        } else if(isReal.equals("false")) {
		    saved.setReal(false);
        }

        saved = strategyRepository.save(newStrategy);
        activeTraders.addTraderFor(saved);

        return saved;
	}
}
