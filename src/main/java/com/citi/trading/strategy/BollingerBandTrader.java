package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of the Bollinger Band trading strategy.
 * //TODO Write description of bolligner band trading strategy
 *
 * @author Owais Khan
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandTrader extends Trader<BollingerBand> {

    private static final Logger LOGGER =
    Logger.getLogger(BollingerBandTrader.class.getName ());


    private double upperBand;
    private double lowerBand;

    public BollingerBandTrader(PricingSource pricing,
                                   OrderPlacer market, StrategyPersistence strategyPersistence) {
        super(pricing, market, strategyPersistence);
    }

    protected int periods(int msec) {
        return msec / 1000 / SECONDS_PER_PERIOD;
    }

    public int getNumberOfPeriodsToWatch() {
        return periods(strategy.getWindowLength());
    }

    /**
     * Helper method to update our flags: which average is greater, or are they
     * exactly or effectively equal?
     */
    private void updateBands(PriceData data) {
        double average = data.getWindowAverage(periods(strategy.getWindowLength()), PricePoint::getClose);
        double standardDeviation = data.getWinoowStandardDeviation(periods(strategy.getWindowLength()), PricePoint::getClose);
        double standardDeviationTimesMultiplier = standardDeviation * strategy.getMultiplier();

        this.upperBand = average + standardDeviationTimesMultiplier;
        this.lowerBand = average - standardDeviationTimesMultiplier;

        LOGGER.fine(String.format("Trader " + strategy.getId() + " as of %s Lower Band: %1.4f,  Upper Band: %1.4f",
                data.getLatestTimestamp(), this.lowerBand, this.upperBand));
    }

    /**
     * When we're open, just check to see if the latest closing price is a profit or
     * loss greater than our configured threshold. If it is, use the base class'
     * <strong>Closer</strong> to close the position.
     */
    protected void handleDataWhenOpen(PriceData data) {
        double currentPrice = data.getData(1).findAny().get().getClose();
        double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
        double profitOrLoss = currentPrice / openingPrice - 1.0;
        if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
            closer.placeOrder(currentPrice);
            if (getStrategy().isReal() && !getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
                profitOrLoss = 0 - profitOrLoss;
            } else {
                List<Position> positions = getStrategy().getPositions();
                if(!positions.get(positions.size() - 1).getOpeningTrade().isBuy()) {
                    profitOrLoss = 0 - profitOrLoss;
                }
            }
            LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.",
                    profitOrLoss * 100));
        }
    }

    /**
     * When we're closed, capture the most recent greater-than/equal flags,
     * update to get the new ones, and see if there's been a change.
     * If short was lower and is now higher, buy to open a long position;
     * if short was higher and is now lower, sell to open a short position.
     */
    protected void handleDataWhenClosed(PriceData data) {
        if (tracking.get()) {
            updateBands(data);
            double currentPrice = data.getData(1).findAny().get().getClose();

            if(currentPrice > this.upperBand) {
                opener.placeOrder(false, data.getData(1).findAny().get().getClose());
                LOGGER.info("Trader " + strategy.getId() + " opening position as current price rose above " +
                        this.upperBand);
            } else if (currentPrice < this.lowerBand) {
                opener.placeOrder(true, data.getData(1).findAny().get().getClose());
                LOGGER.info("Trader " + strategy.getId() + " opening position as current price dropped below " +
                        this.lowerBand);
            }

        } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
            updateBands(data);
            tracking.set(true);
            LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
        }
    }
}
