package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * Represents a Bollinger Band trading strategy, adding the parameters
 * that are specific to that algorithm.
 *
 * @author Owais Khan
 */
@Entity
@DiscriminatorValue("B")
public class BollingerBand extends Strategy implements Serializable {
    private static final long serialVersionUID = 1L;

    private int windowLength;
    private double multiplier;
    private double exitThreshold;

    public BollingerBand() {
    }

    public BollingerBand(String stock, int size, int windowLength, double multiplier, double exitThreshold) {
        super(stock, size);
        this.windowLength = windowLength;
        this.multiplier = multiplier;
        this.exitThreshold = exitThreshold;
    }

    public double getExitThreshold() {
        return this.exitThreshold;
    }

    public void setExitThreshold(double exitThreshold) {
        this.exitThreshold = exitThreshold;
    }

    public int getWindowLength() {
        return this.windowLength;
    }

    public void setWindowLength(int windowLength) {
        this.windowLength = windowLength;
    }

    public double getMultiplier() { return this.multiplier; }

    public void setMultiplier(double multiplier) { this.multiplier = multiplier; }

    @Override
    public String toString() {
        return String.format("BollingerBands: [window=%d, exit=%1.4f, %s]",
                windowLength, exitThreshold, stringRepresentation());
    }
}
