# High Frequency Trader

## CI/CD Infrastructure
The project consists of an Angular backend and a Spring boot backend. The project is built
using a jenkins pipeline and then deployed to an Openshift server for public access.

Openshift credentials:
* Project Name: redtrader
* Username: teamred
* Password: admin